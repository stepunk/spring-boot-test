package stepan.myasnykh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import stepan.myasnykh.model.Artist;

import java.math.BigInteger;
import java.util.List;

public interface ArtistRepository extends JpaRepository<Artist, BigInteger> {
    List<Artist> findAll();

}
