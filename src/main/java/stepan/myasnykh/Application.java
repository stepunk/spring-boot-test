package stepan.myasnykh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}

//public class Application implements CommandLineRunner {
//
//    @Autowired
//    ArtistRepository repository;
//
//    @PersistenceContext
//    EntityManager entityManager;
//
//    @Autowired
//    EntityManagerFactory factory;
//    public static void main(String[] args) {
//        SpringApplication.run(Application.class, args);
//    }
//
//    @Override
//    public void run(String... args) throws Exception {
//        Artist build = Artist.builder().id(BigInteger.ONE).build();
//        repository.save(build);
//    }
//}