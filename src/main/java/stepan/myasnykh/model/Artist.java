package stepan.myasnykh.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "Artist")
@NoArgsConstructor
@Data
@AllArgsConstructor
@Builder
@ToString(of = {"name"})
@EqualsAndHashCode(of = {"id"})
public class Artist {

    @Id
    private BigInteger id;

    //    @Column(name = "name")
    private String name;

}
