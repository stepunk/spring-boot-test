FROM openjdk:8
ADD spring-boot-test-1.0-SNAPSHOT.jar .

ENTRYPOINT exec java -jar spring-boot-test-1.0-SNAPSHOT.jar
